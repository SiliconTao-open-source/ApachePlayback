#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>

/***************************
Modified by @: 4
Royce June 14, 2012: Added exit codes back in. Make program run in single socket session for each iteration; requires crafted log for playback.


To compile this program.
D=$(date +%F-%H%M); gcc apb.c -o apb_${D} -lpthread
***************************/

#define MAXFIELDS 64
#define LOG_MIN_FIELDS 9	/* minimum number of fields in a logfile line */

#define MIN(a,b) ((a)<(b)?(a):(b))

int ThreadStackSize = 1048576;
volatile int ThreadCount = 0;
int MaxSleepTime = 99999999;
volatile int OpenSocketCount = 0;
int MaxOpenSockets = 0;
int MaxActiveThreads = 0;
time_t Starttime;

char *Server = NULL;
struct hostent *Hostp = NULL;
int ServerPort = 80;
int Iterations = 1;

// For cookie support
int UseCookie = 0;
char *CookieFile = NULL;
char *CookieContent = NULL;

struct http_request {
	char *command;
	char *url;
	char *host;
	long time;
};

int
thread_create(pthread_t *thread_id, void *(*start)(void *), void *arg, int detach)
{
	pthread_t dummythread, *threadarg;
	pthread_attr_t threadAttrs;

	if(thread_id) threadarg = thread_id;
		else threadarg = &dummythread;

	pthread_attr_init(&threadAttrs);
	if(detach) pthread_attr_setdetachstate(&threadAttrs,PTHREAD_CREATE_DETACHED);
	if(ThreadStackSize) pthread_attr_setstacksize(&threadAttrs,ThreadStackSize);

	return(pthread_create(threadarg,&threadAttrs,start,arg));
}


int Close(int OldSocket)
{
	if((OpenSocketCount > 0) && (OldSocket != 0))
	{
		OpenSocketCount--;
		printf("OpenSocketCount = %d, MaxOpenSockets = %d\n", OpenSocketCount, MaxOpenSockets);
		return(close(OldSocket));
	}
}

int
connect_to(char *servername, int port)
{
	int fd;
	struct sockaddr_in server;
	int retval;
	struct hostent *hostp = NULL;

	if(Hostp) {
		server.sin_addr.s_addr = ((struct in_addr *)Hostp->h_addr)->s_addr;
		server.sin_port = htons(port);
		server.sin_family = AF_INET;
	} else {
		if((hostp = gethostbyname(servername)) == NULL) {
			printf("server %s: %s\n",servername,hstrerror(h_errno));
			fprintf(stderr,"server %s: %s\n",servername,hstrerror(h_errno));
			exit(99);
		}
		server.sin_addr.s_addr = ((struct in_addr *)hostp->h_addr)->s_addr;
		server.sin_port = htons(port);
		server.sin_family = AF_INET;
	}
	if((fd = socket(AF_INET,SOCK_STREAM,0)) < 0) {
		perror("socket");
		fprintf(stdout,"socket error\n");
		exit(108);
		return(fd);
	}
	OpenSocketCount++;
	//printf("OpenSocketCount = %d\n", OpenSocketCount);
	if(MaxOpenSockets < OpenSocketCount)
	{
		MaxOpenSockets = OpenSocketCount;
	}
	printf("OpenSocketCount = %d, MaxOpenSockets = %d\n", OpenSocketCount, MaxOpenSockets);
	if((retval = connect(fd,(struct sockaddr *)&server,sizeof(server))) < 0) 
	{
		char TempBuff[1024];
		memset(TempBuff, 0, 1024);
		snprintf(TempBuff, 1023, "connect %s", servername);
		//printf("%s\n", TempBuff);
		//fprintf(stderr ,"What is going on here, %s\n", TempBuff);
		perror(TempBuff);
		//perror("connect");
		Close(fd);
		exit(128);
		//return(retval);
	}
	return(fd);
}

struct tabsplit {
	int nfields;
	char *fields[MAXFIELDS];
};

void
tabsplit_free(struct tabsplit *tsp)
{
	int x;

	for(x=0; x < tsp->nfields; x++) free(tsp->fields[x]);
	free(tsp);
}

unsigned long
get_millitime()
{
	struct timeval tv;

	gettimeofday(&tv,NULL);
	return((tv.tv_sec * 1000)+(tv.tv_usec/1000));
}

long
ap_to_millis(char *what)
{
	long retval;

	// what += 11; */
	retval = ((atoi(what)*3600) + (atoi(what+3)*60) + atoi(what+6)) * 1000;
	return(retval);
}

char *
strsave(char *what)
{
	char *retval;

	if((retval = (char *)malloc(strlen(what)+1)) != NULL) strcpy(retval,what);
	return(retval);
}

void
stripcr(char *what)
{
	char *ptr;

	if((ptr = strrchr(what,'\r')) != NULL) *ptr = '\0';
}

void *
http_get(void *dummy)
{
	char *server,*url;
	char buf[BUFSIZ*4];
	char results[BUFSIZ];
	char *ptr;
	int fd, nc;
	unsigned long stime, etime;
	FILE *sockp;
	int x, iline;
	struct tabsplit *tsp;
	time_t duration;
	int readbytes;

	ThreadCount++;
	if(MaxActiveThreads < ThreadCount)
	{
		MaxActiveThreads = ThreadCount;
	}
	//printf("ThreadCount = %d\n", ThreadCount);
	printf("ThreadCount = %d, MaxActiveThreads = %d\n", ThreadCount, MaxActiveThreads);

	tsp = (struct tabsplit *)dummy;

	server = tsp->fields[1];
	if((ptr = strchr(server,'/')) != NULL) *ptr = '\0';

	url = tsp->fields[6];

	// fprintf(stderr,"GET called on basepage <%s>, url <%s>\n",server,url);
	for(x = 0; x < Iterations; x++) 
	{
		if((fd = connect_to(server,ServerPort)) < 0) return;
		if((sockp = fdopen(fd,"r+")) == NULL) 
		{
			perror("fdopen socket");
			Close(fd);
			ThreadCount--;
			printf("ThreadCount = %d\n", ThreadCount);
			return;
		}
		// use the cookie here
		if(UseCookie)
		{
			sprintf(buf,"GET %s HTTP/1.1\nHost: %s\nUser-Agent: APB/1.1\nCookie: %s\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\nAccept-Language: en-us,en;q=0.5\nConnection: close\n\n",url,server,CookieContent);
		} else
		{
			sprintf(buf,"GET %s HTTP/1.1\nHost: %s\nUser-Agent: APB/1.1\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\nAccept-Language: en-us,en;q=0.5\nConnection: close\n\n",url,server);
		}
		stime = get_millitime();
		if(write(fd,buf,strlen(buf)) < 0) 
		{
			perror("write");
			Close(fd);
			ThreadCount--;
			printf("ThreadCount = %d, MaxActiveThreads = %d\n", ThreadCount, MaxActiveThreads);
			return;
		}
		/* read full header */
		iline = 0;
		while(fgets(buf,BUFSIZ,sockp)) 
		{
			stripcr(buf);
			if(!iline) strcpy(results,buf);
			iline++;
			if(buf[0] == '\0') break;
		}
		readbytes = 0;

		while((nc = fread(buf,1,BUFSIZ,sockp)) > 0)
		{
			readbytes += nc; /* write(1,buf,nc); throw it away */
		}
		duration = time(NULL) - Starttime;
		// fprintf(stderr,"%02d:%02d:%02d GET at %s <%s (%s)> <%s> %ld msec\n",
		printf("%02d:%02d:%02d\tGET\t%s\t%s\t%s\t%s\t%s\t%db\t%ld msec\n",
			(duration/3600),
			((duration % 3600) / 60),
			((duration % 3600) % 60),
			tsp->fields[0],url,tsp->fields[3],tsp->fields[4],results,readbytes,get_millitime() - stime);
		Close(fd);
	}
	tabsplit_free(tsp);
	ThreadCount--;
	printf("ThreadCount = %d, MaxActiveThreads = %d\n", ThreadCount, MaxActiveThreads);
}

int
main(int argc, char *argv[])
{
	extern char *optarg;

	FILE *lf;
	int c;
	char buf[BUFSIZ];
	int lineno = 0;
	int fmterrs = 0;
	char *ptr, *tptr, *cmd;
	struct tabsplit *args;
	int nfield;
	long lasttime = -1;
	long starttime = -1;
	long sleeptime;
	long ctime;
	int maxgets = 0;
	int curgets = 0;

	Starttime = time(NULL);

	while((c=getopt(argc,argv,"s:p:i:t:m:w:c:")) != -1) 
	{
		printf("argument = '%c'\n", c);
		switch(c) 
		{
			case 's':
			Server = strsave(optarg);
			break;

			case 'p':
			ServerPort = atoi(optarg);
			break;

			case 'i':
			Iterations = atoi(optarg);
			break;

			case 't':
			starttime = ap_to_millis(optarg);
			break;

			case 'm':
			maxgets = atoi(optarg);
			break;

			case 'w':
			MaxSleepTime = atoi(optarg);
			break;

			case 'c':
			CookieFile = strsave(optarg);
			UseCookie = 1;			
			break;

			default:
			fprintf(stderr,"usage: %s [-sservername][-pport][-iiterations][-tstarttime][-mmaxgets][-wmaxsleeptime][-ccookie]\n",argv[0]);
			fprintf(stderr,"	-s server\tapb connects to server\n");
			fprintf(stderr,"	-p port\t\tconnect to port instead of 80\n");
			fprintf(stderr,"	-i iter\t\tperform iter simultaneous iterations of each request\n");
			fprintf(stderr,"	-t starttime\tcommence playback at startime in the log file\n");
			fprintf(stderr,"	-m maxgets\tstop after maxgets GET requests\n");
			fprintf(stderr,"	-w maxsleeptime\tlimit inter-request sleeps to maxsleeptime milliseconds\n");
			fprintf(stderr,"	-c cookie file\t\tUse this cookie file while making connections.\n");

			exit(38);
		}
	}

	if(UseCookie)
	{	
		FILE *fp;
		if(fp = fopen ( CookieFile, "rb" ))
		{
			char TempBuffer[8192];
			int CookieLen = fread(TempBuffer, 1, 8191, fp);
			// printf("CookieLen = %d\n", CookieLen);
			TempBuffer[CookieLen] = '\0';
			CookieContent = strsave(TempBuffer);
			fclose(fp);
			// printf("This is our cookie = '%s'\n", CookieContent);
		} else
		{
			fprintf(stderr,"Cannot open cookie file '%s' for reading.\n", CookieFile);
		}
	} 

	if(Server) {
		if((Hostp = gethostbyname(Server)) == NULL) {
			//printf("server %s: %s\n",Server,hstrerror(h_errno));
			fprintf(stderr,"server %s: %s\n",Server,hstrerror(h_errno));
			exit(64);
		}
	}

	buf[BUFSIZ-1] = '\0';
	while(fgets(buf,BUFSIZ-1,stdin)) {
		lineno++;
		if(!isdigit(buf[0])) {
badformat:
			fprintf(stderr,"bad format at line %d\n",lineno);
			continue;
		}
		ptr = &buf[0];
		if((args = (struct tabsplit *)malloc(sizeof(struct tabsplit))) == NULL) {
			perror("malloc");
			printf("malloc error\n");
			exit(80);
		}
		args->nfields = 0;
		while((tptr = strtok(ptr,"\t\n")) != NULL) {
			args->fields[args->nfields] = strsave(tptr);
			// printf("field %d is <%s>\n",args.nfields,args.fields[args.nfields]);
			args->nfields++;
			ptr = NULL;
		}
		if(args->nfields < LOG_MIN_FIELDS) goto badformat;

		cmd = args->fields[5];
		ctime = ap_to_millis(&args->fields[0][11]);
		if(ctime < starttime) {
			// fprintf(stderr,"skipping time on line %d\n",lineno);
			continue;
		}
		if(lasttime == -1) {
			lasttime = ap_to_millis(&args->fields[0][11]);
		}
		if(!strcmp(cmd,"GET")) {
			curgets++;
			if(maxgets) {
				if(curgets > maxgets) break;
			}
			if(ctime > lasttime) {
				sleeptime = ctime - lasttime;
				sleeptime = MIN(sleeptime, MaxSleepTime);
				fprintf(stderr,"sleeping at line %d, time %ld\n",lineno,sleeptime);
				sleep(sleeptime/1000);
				lasttime = ctime;
			}
			if(thread_create(NULL,http_get,(void *)args,1) != 0) {
				perror("thread_create");
				printf("thread_create error\n");
				exit(15);
			}
		} else if(!strcmp(cmd,"POST")) {
			// printf("line %d: POST <%s>\n",lineno,args.fields[6]);
			if((tptr = strchr(args->fields[6],'?')) != NULL) *tptr = '\0';
			// printf("POST %s\n",args.fields[6]);
		} else if (!strcmp(cmd,"HEAD")) {
			/* do nothing - we ignore this */
		} else if (!strcmp(cmd,"OPTIONS")) {
			/* do nothing - we ignore this */
		} else {
			fprintf(stderr,"Unknown cmd <%s> on %d\n",cmd,lineno);
		}
	}
	fprintf(stderr,"complete, read %d lines, processed %d GETs\n",lineno,curgets);
	while(ThreadCount > 0) {
		fprintf(stderr,"waiting for threads, %d still running\n",ThreadCount);
		sleep(10);
	}
	exit(0);
}

