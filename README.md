# ApachePlayback

Apache Playback reads Apache log files then mimics the user activity.

This was developed for load testing.

Multiple load servers can be used to increase demand on the tested system.

[Source files on GitLab](https://gitlab.com/SiliconTao-open-source/ApachePlayback)

